﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    Transform playerTransform;
    public int moveSpeed = 4;

    void Update()
    {
        PlayerTracker();
    }

    void PlayerTracker()
    {
        GetPlayerPosition();

        //Enemy mira en direcció a la posició de Player
        transform.LookAt(playerTransform);

        //Enemy avança directament cap a Player
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
        FixTransform();

    }

    void GetPlayerPosition()
    {
        //Busca el GameObject Player i playerTransform obté el seu Transform, la seva posició
        GameObject go = GameObject.Find("Player");
        playerTransform = go.transform;
    }

    void FixTransform()
    {
        //Manté el sprite estatic i dret
        var playerRotation = transform.rotation;
        playerRotation.x = 0;
        playerRotation.y = 0;
        playerRotation.z = 0;
        transform.rotation = playerRotation;

        //Actualitza la Z per superposarse al Player
        var playerPosition = transform.position;
        playerPosition.z = 0;
        transform.position = playerPosition;
    }
}
