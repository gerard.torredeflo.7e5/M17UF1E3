﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefeat : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        GameManager.Instance.score += 5;
        Destroy(gameObject);
    }
    private void OnMouseDown()
    {
        GameManager.Instance.score += 5;
        Destroy(gameObject);
    }
}
