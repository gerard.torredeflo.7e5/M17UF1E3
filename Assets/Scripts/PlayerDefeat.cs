﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDefeat : MonoBehaviour
{
    PlayerData lifes;
    void Start()
    {
        lifes = GetComponent<PlayerData>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        lifes.vides -= 1;
    }

}
