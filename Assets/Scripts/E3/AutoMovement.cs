﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovement : MonoBehaviour
{
    SpriteRenderer spriteFlip;
    PlayerData speed, weight;
    float velocitatReal;

    // Start is called before the first frame update
    void Start()
    {
        spriteFlip = GetComponent<SpriteRenderer>();
        speed = GetComponent<PlayerData>();
        weight = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        ProporcioInversaVelocitatPes();
        Rutina();
    }

    bool anarEsquerra = true;
    void Rutina()
    {
        if (anarEsquerra == true)
        {
            spriteFlip.flipX = false;
            var posicio = transform.position;
            posicio.x -= velocitatReal;
            transform.position = posicio;
            if (transform.position.x <= -8)
            {
                anarEsquerra = false;
            }
        }

        if (anarEsquerra == false)
        {
            spriteFlip.flipX = true;
            var posicio = transform.position;
            posicio.x += velocitatReal;
            transform.position = posicio;
            if (transform.position.x >= 8)
            {
                anarEsquerra = true;
            }
        }
    }

    void ProporcioInversaVelocitatPes()
    {
        float X;
        float Y;
        velocitatReal = speed.velocitat;
        Y = velocitatReal;
        X = weight.pes;

        //Calcul proporcionalitat inversa
        Y = Y / (X * 5);

        velocitatReal = Y;
    }
}
