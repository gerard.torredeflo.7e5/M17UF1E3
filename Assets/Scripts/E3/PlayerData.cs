﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerTipus
{
    Mage,
    Warrior,
    Templar,
    Assassin
}

public class PlayerData : MonoBehaviour
{
    public int vides;
    private string nom;
    public string cognom;
    public PlayerTipus PTipus;
    public float alçada, pes, velocitat;
    public int distanciaRecorregut;
}
