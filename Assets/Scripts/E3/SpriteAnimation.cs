﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public Sprite[] sprites;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        //FrameRate fixat
        Application.targetFrameRate = 12;
        QualitySettings.vSyncCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SpriteLoop();
    }

    int swapSprite = 2;
    int lastSprite = 1;
    void SpriteLoop()
    {
        if (swapSprite == 2)
        {
            spriteRenderer.sprite = sprites[1];
            if (lastSprite == 1)
            {
                swapSprite = 3;
            }
            if (lastSprite == 3)
            {
                swapSprite = 1;
            }
            //Debug.Log("Sprite idle cargado");
        }
        if (swapSprite == 3)
        {
            spriteRenderer.sprite = sprites[2];
            lastSprite = 3;
            swapSprite = 2;
            //Debug.Log("Sprite andar1 cargado");
        }
        if (swapSprite == 1)
        {
            spriteRenderer.sprite = sprites[0];
            lastSprite = 1;
            swapSprite = 2;
            //Debug.Log("Sprite andar2 cargado");
        }
    }
}
