﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudFPS : MonoBehaviour
{
    public float temps, refresca, avgFramerate;
    string display = "{0} FPS";
    private Text fpsTxt;

    private void Start()
    {
        fpsTxt = GetComponent<Text>();
    }

    private void Update()
    {
        float timelapse = Time.smoothDeltaTime;
        temps = temps <= 0 ? refresca : temps -= timelapse;

        if (temps <= 0) avgFramerate = (int)(1f / timelapse);
        fpsTxt.text = string.Format(display, avgFramerate.ToString());
    }
}
