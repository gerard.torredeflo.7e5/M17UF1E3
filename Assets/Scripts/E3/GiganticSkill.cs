﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiganticSkill : MonoBehaviour
{
    PlayerData height;
    float alçada;
    // Start is called before the first frame update
    void Start()
    {
        height = GetComponent<PlayerData>();
        alçada = height.alçada;
        ProporcioDirectaAlçada();
    }

    // Update is called once per frame
    void Update()
    {
        GiantSkill();
    }

    bool activeCooldown = false;
    void GiantSkill()
    {
        if (Input.GetKeyDown(KeyCode.Q) && activeCooldown != true)
        {
            activeCooldown = true;
            StartCoroutine(Creixement());
        }
    }

    IEnumerator Creixement()
    {
        float maxAlçada = 16;
        while (alçada < maxAlçada)
        {
            alçada += 1;
            ProporcioDirectaAlçada();
            yield return new WaitForSeconds(0);
        }
        StartCoroutine(Decreixement());
    }

    IEnumerator Decreixement()
    {
        float minAlçada = alçada / 2;
        yield return new WaitForSeconds(5);
        while (alçada > minAlçada)
        {
            alçada -= 1;
            ProporcioDirectaAlçada();
            yield return new WaitForSeconds(0);
        }
        StartCoroutine(Cooldown());
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(8);
        activeCooldown = false;
    }

    void ProporcioDirectaAlçada()
    {
        var alçadaR = transform.localScale;
        alçadaR.x = alçadaR.y = alçada;
        transform.localScale = alçadaR;
    }

    /*void MantenirNivell()
    {
        //Intent de mantindre la posició en pantalla
        //NOMES FUNCIONA AMB ALTURA 8 Y POSICIO -2
        var mantenirNivell = transform.position;
        mantenirNivell.y = mantenirNivell.y / 2;
        transform.position = mantenirNivell;
    }*/
}
