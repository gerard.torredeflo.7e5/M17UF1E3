﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemyPrefab;
    [SerializeField]
    int spawnDelay;
    GameObject[] acEnemies;
    public int currentEnemies;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        acEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        currentEnemies = acEnemies.Length;
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(spawnDelay);
        int spawnMargin;
        spawnMargin = Random.Range(1, 4);
        Vector2 randomPositionOnScreen;
        switch (spawnMargin)
        {
            case 1:
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(0.05f, Random.value));
                Instantiate(enemyPrefab, randomPositionOnScreen, Quaternion.identity);
                break;
            case 2:
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(0.95f, Random.value));
                Instantiate(enemyPrefab, randomPositionOnScreen, Quaternion.identity);
                break;
            case 3:
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, 0.05f));
                Instantiate(enemyPrefab, randomPositionOnScreen, Quaternion.identity);
                break;
            case 4:
                randomPositionOnScreen = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, 0.95f));
                Instantiate(enemyPrefab, randomPositionOnScreen, Quaternion.identity);
                break;

        }
        StartCoroutine(Spawn());
    }
}
