﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptUI : MonoBehaviour
{
    private string scoreText;
    private string lifeText;
    private string counterText;

    void Start()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>().text;
        lifeText = GameObject.Find("LifeText").GetComponent<Text>().text;
        counterText = GameObject.Find("CounterText").GetComponent<Text>().text;
    }

    void Update()
    {
        counterText = "Enemics: " + GameManager.Instance.currentEnemies;
        lifeText = GameManager.Instance.currentLifes + " <3";
        scoreText = "SCORE: " + GameManager.Instance.score;
        GameObject.Find("ScoreText").GetComponent<Text>().text = scoreText;
        GameObject.Find("LifeText").GetComponent<Text>().text = lifeText;
        GameObject.Find("CounterText").GetComponent<Text>().text = counterText;
    }
}
