﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null) Debug.LogError("Game Manager is NULL!!");
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public int currentLifes;
    public int currentEnemies;
    public int score;

    void Update()
    {
        currentLifes = GameObject.Find("Player").GetComponent<PlayerData>().vides;
        currentEnemies = GameObject.Find("EnemySpawner").GetComponent<EnemySpawn>().currentEnemies;
        playerLifeCheck();
    }

    void playerLifeCheck()
    {
        if (currentLifes <= 0) SceneManager.LoadScene("M17-UF1-E4");
    }
}
